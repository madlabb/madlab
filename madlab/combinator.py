import numpy as np
from .descriptor import BaseDescriptor, CombinedDescriptor
from .element import available_properties
from .functions import available_single_functions, available_pair_functions


class Combinator(object):

    def __init__(self, recursive_level=2, exclude_properties=[]):

        self.recursive_level = recursive_level
        self.exclude_properties = exclude_properties
        self.generate_base_descriptors()
        self.generate_combinations()

    def generate_base_descriptors(self):
        base_descriptors = []
        for property_name in available_properties:
            if property_name in self.exclude_properties:
                continue
            base_descriptors.append(BaseDescriptor(property_name))
        self.base_descriptors = base_descriptors

    def generate_combinations(self):
        # add base level
        descriptors = self.base_descriptors
        self.descriptors = descriptors
        for _ in range(self.recursive_level):
            self._take_recursive_step()

    def _take_recursive_step(self):
        # add single function level
        new_descriptors = []
        for descriptor in self.descriptors:
            for function in available_single_functions:
                new_descriptors.append(CombinedDescriptor(function, descriptor))

        # add pair function level
        for i in range(len(self.descriptors)):
            for j in range(i+1, len(self.descriptors)):
                for function in available_pair_functions:
                    d1 = self.descriptors[i]
                    d2 = self.descriptors[j]
                    new_descriptors.append(CombinedDescriptor(function, d1, d2))
        
        for descriptor in self.descriptors:
            for function in available_single_functions:
                new_descriptors.append(CombinedDescriptor(function, descriptor))

        self.descriptors.extend(new_descriptors)

    def get_atomic_description(self, element):
        row = []
        for descriptor in self.descriptors:
            row.append(descriptor(element))
        for i, r in enumerate(row):
            if np.isnan(r) or np.isinf(r) or r is None or abs(r) > 1e60:
                row[i] = 0
        return row
