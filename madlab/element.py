

available_properties = ['mass',
                        'Z',
                        'covalent_radii',
                        'gs_magnetic_moment',
                        'vdw_radii',
                        'symmetry',
                        'tmelt',
                        'electronegativity',
                        'ionization_energy']


class Element(object):

    list_of_properties = ['mass']

    def __init__(self, symbol, aps=None):
        self.symbol = symbol
        if aps is None:
            self.aps = dict()
        else:
            self.aps = aps

    def __getitem__(self, property_name):
        return self.aps[property_name]

    def __str__(self):
        nice_str = 'Element: {}. '.format(self.symbol)
        for key in sorted(self.aps.keys()):
            nice_str += '{} : {} '.format(key, self.aps[key])
        return nice_str
