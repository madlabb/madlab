import numpy as np


def add(a, b):
    return a+b


def minus(a, b):
    return a-b


def multiply(a, b):
    return a*b


def divide(a, b):
    return a/b


def sqrt(a):
    return np.sqrt(a)


def pow2(a):
    return a**2


def log(a):
    return np.log(a)


def exp(a):
    return np.exp(a)

available_pair_functions = [add, minus, multiply, divide]
available_single_functions = [sqrt, pow2, log, exp]
