class BaseDescriptor(object):

    def __init__(self, name):
        self.property_name = name

    def __call__(self, element):
        return element[self.property_name]

    def __str__(self):
        return self.property_name

    def __repr__(self):
        return str(self)


class CombinedDescriptor(object):

    def __init__(self, function, descriptor1, descriptor2=None):
        self.function = function
        self.descriptor1 = descriptor1

        if descriptor2 is None:
            self.single = True
        else:
            self.descriptor2 = descriptor2
            self.single = False

    def __call__(self, element):
        if self.single:
            value = self.function(self.descriptor1(element))
        else:
            value = self.function(self.descriptor1(element), self.descriptor2(element))

        return value

    def __str__(self):
        if self.single:
            return '{}({})'.format(self.function.__name__, self.descriptor1)
        else:
            return '{}({}, {})'.format(self.function.__name__, self.descriptor1, self.descriptor2)

    def __repr__(self):
        return str(self)
