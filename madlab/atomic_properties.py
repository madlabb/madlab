import ase
from madlab.element import Element
from madlab.reference_datasets.melting_temperature import t_melt
from madlab.reference_datasets.electronegativity import electronegativity

masses = ase.data.atomic_masses
atomic_numbers = ase.data.atomic_numbers
covalent_radii = ase.data.covalent_radii
gs_magnetic_moments = ase.data.ground_state_magnetic_moments
vdw_radii = ase.data.vdw_radii
reference_states = ase.data.reference_states

elements = {}

for z, s in enumerate(atomic_numbers):
    aps = {}
    z = z+1
    try:
        aps['mass'] = masses[z]
    except Exception as e:
        aps['mass'] = None
    try:
        aps['Z'] = z
    except Exception as e:
        aps['Z'] = None
    try:
        aps['covalent_radii'] = covalent_radii[z]
    except Exception as e:
        aps['covalent_radii'] = None
    try:
        aps['gs_magnetic_moment'] = gs_magnetic_moments[z]
    except Exception as e:
        aps['gs_magnetic_moment'] = None
    try:
        aps['symmetry'] = reference_states[z]['symmetry']
    except Exception as e:
        aps['symmetry'] = None
    try:
        aps['vdw_radii'] = vdw_radii[z]
    except Exception as e:
        aps['vdw_radii'] = None
    try:
        aps['tmelt'] = t_melt[z]
    except Exception as e:
        aps['tmelt'] = None
    try:
        aps['electronegativity'] = electronegativity[z]
    except Exception as e:
        aps['electronegativity'] = None

    elements[s] = Element(s, aps=aps)
