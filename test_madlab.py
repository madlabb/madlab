import numpy as np
from sklearn.linear_model import Lasso
from madlab import Combinator
from madlab.atomic_properties import elements
from madlab.element import available_properties


def lasso_l0(A, y):
    # train
    lasso = Lasso(alpha=10000, fit_intercept=False, max_iter=50000)
    lasso.fit(A, y)

    features = np.where(lasso.coef_ != 0)[0]
    mse_vals, x_vals = [], []
    for f in features:
        A_f = A[:, f].reshape(A.shape[0], 1)
        x = np.linalg.lstsq(A_f, y)[0]
        mse = sum((np.dot(A_f, x) - y)**2)
        mse_vals.append(mse)
        x_vals.append(x[0])
    return features[np.argmin(mse_vals)], x_vals[np.argmin(mse_vals)]


include_properties = ['mass', 'covalent_radii', 'vdw_radii', 'Z']
exclude_properties = list(set(available_properties) - set(include_properties))

element_list = []
for element in elements.values():
    for prop in include_properties+['tmelt']:
        if element[prop] is None or np.isnan(element[prop]):
            break
    else:
        if element['tmelt'] > 300:
            element_list.append(element)


# generate descriptors
combi = Combinator(recursive_level=1, exclude_properties=exclude_properties)

adp_matrix, target_values = [], []
for element in element_list:
    adp_matrix.append(combi.get_atomic_description(element))
    target_values.append(element['tmelt']+273)
adp_matrix = np.array(adp_matrix)
target_values = np.array(target_values)

# train
lasso = Lasso(alpha=10, fit_intercept=False)
lasso.fit(adp_matrix, target_values)


winning_index, x = lasso_l0(adp_matrix, target_values)
print('Winning L0 feature', combi.descriptors[winning_index])

# plot
import matplotlib.pyplot as plt
plt.plot(x*adp_matrix[:, winning_index], target_values, 'o')

plt.plot(np.dot(adp_matrix, lasso.coef_), target_values, 'o')
plt.show()
