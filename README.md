MADLab
======
Materials atomic description labratory


``` python
# setup
elements, melting_points = madlab.reference_datasets.get_melting_points()

# get adp
atomic_descriptor = AtomicDescriptor(recursive_level=4, exclude=['mass'])
ad_matrix = atomic_descriptor.get_atomic_description(elements)

# train
lasso = Lasso(alpha=0.5)
lasso.fit(ad_matrix, target_values)
```

![](examples/temperature_prediction.png)  